using System;
using Masakura.Unity.JUnit.Reporter.Times;

namespace Masakura.Unity.JUnit.Reporter.Tests.Times
{
    internal sealed class MockTimeProvider : TimeProvider
    {
        private readonly DateTimeOffset _offset;

        public MockTimeProvider(DateTimeOffset offset)
        {
            _offset = offset;
        }

        public override DateTimeOffset GetLocalNow()
        {
            return _offset;
        }
    }
}