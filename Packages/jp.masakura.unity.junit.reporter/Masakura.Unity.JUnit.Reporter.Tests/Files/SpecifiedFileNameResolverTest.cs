using Masakura.Unity.JUnit.Reporter.Files;
using NUnit.Framework;

namespace Masakura.Unity.JUnit.Reporter.Tests.Files
{
    public sealed class SpecifiedFileNameResolverTest
    {
        [Test]
        public void TestResolve()
        {
            var target = new SpecifiedFileNameResolver("report.xml");
            
            Assert.That(target.Resolve(), Is.EqualTo("report.xml"));
        }
    }
}