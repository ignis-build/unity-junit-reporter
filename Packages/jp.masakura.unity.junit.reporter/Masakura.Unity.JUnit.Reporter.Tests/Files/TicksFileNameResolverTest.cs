using System;
using Masakura.Unity.JUnit.Reporter.Files;
using Masakura.Unity.JUnit.Reporter.Tests.Times;
using NUnit.Framework;

namespace Masakura.Unity.JUnit.Reporter.Tests.Files
{
    public sealed class TicksFileNameResolverTest
    {
        [Test]
        public void TestResolve()
        {
            var now = DateTimeOffset.Now;
            var resolver = new MockTimeProvider(now);
            var target = new TicksFileNameResolver(resolver);
            
            Assert.That(target.Resolve(),
                Is.EqualTo($"TestResults-junit-{now.Ticks}.xml"));
        }
    }
}