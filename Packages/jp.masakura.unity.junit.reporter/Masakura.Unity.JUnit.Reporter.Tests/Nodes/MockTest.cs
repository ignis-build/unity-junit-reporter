using System;
using System.Collections.Generic;
using NUnit.Framework.Interfaces;
using UnityEditor.TestTools.TestRunner.Api;
using RunState = UnityEditor.TestTools.TestRunner.Api.RunState;

namespace Masakura.Unity.JUnit.Reporter.Tests.Nodes
{
    internal sealed class MockTest : ITestAdaptor
    {
        public string Id { get; private set; }
        public string Name { get; private set; }
        public string FullName { get; private set; }
        public int TestCaseCount { get; private set; }
        public bool HasChildren => throw new NotSupportedException();
        public bool IsSuite { get; private set; }
        public IEnumerable<ITestAdaptor> Children => throw new NotSupportedException();
        public ITestAdaptor Parent { get; private set; }
        public int TestCaseTimeout => throw new NotSupportedException();
        public ITypeInfo TypeInfo => throw new NotSupportedException();
        public IMethodInfo Method => throw new NotSupportedException();
        public string[] Categories => throw new NotSupportedException();
        public bool IsTestAssembly { get; private set; }
        public RunState RunState => throw new NotSupportedException();
        public string Description => throw new NotSupportedException();
        public string SkipReason => throw new NotSupportedException();
        public string ParentId => throw new NotSupportedException();
        public string ParentFullName => throw new NotSupportedException();
        public string UniqueName => throw new NotSupportedException();
        public string ParentUniqueName => throw new NotSupportedException();
        public int ChildIndex => throw new NotSupportedException();
        public TestMode TestMode => throw new NotSupportedException();

        public static MockTest TestClass(string className)
        {
            return new MockTest
            {
                FullName = className,
                IsSuite = true,
                IsTestAssembly = false
            };
        }

        public static ITestAdaptor TestCase(string testCaseName)
        {
            return new MockTest
            {
                Name = testCaseName,
                IsSuite = false,
                IsTestAssembly = false
            };
        }

        public static ITestAdaptor TestAssembly(string id,
            int testCaseCount,
            string assemblyName)
        {
            return new MockTest
            {
                Id = id,
                TestCaseCount = testCaseCount,
                Name = assemblyName,
                IsSuite = true,
                IsTestAssembly = true
            };
        }

        public static ITestAdaptor TestNamespace(string @namespace)
        {
            return new MockTest
            {
                Name = @namespace,
                IsSuite = true,
                IsTestAssembly = false
            };
        }

        public ITestAdaptor WithParent(ITestAdaptor parent)
        {
            Parent = parent;
            return this;
        }
    }
}