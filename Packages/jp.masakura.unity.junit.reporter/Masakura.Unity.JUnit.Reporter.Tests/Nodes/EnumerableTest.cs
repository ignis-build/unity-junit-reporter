﻿using System.Linq;
using Masakura.Unity.JUnit.Reporter.Nodes;
using NUnit.Framework;
using static Masakura.Unity.JUnit.Reporter.Tests.Nodes.MockTestResult;

namespace Masakura.Unity.JUnit.Reporter.Tests.Nodes
{
    public sealed class EnumerableTest
    {
        private MockTestResult _testCase1;
        private MockTestResult _testCase2;
        private MockTestResult _testCase3;

        [SetUp]
        public void SetUp()
        {
            _testCase1 = TestCase("Test1", 0.982).WithPassed();
            _testCase2 = TestCase("Test2", 0.102).WithPassed();
            _testCase3 = TestCase("Test3", 0.103).WithPassed();
        }

        [Test]
        public void TestTestCases()
        {
            var target =
                TestAssembly("1028", "Foo.Bar.dll",
                    6, 3, 2, 1, 10,
                    0.0335219, "2023-10-30T08:26:08",
                    new[]
                    {
                        TestNamespace("Foo", new[]
                        {
                            TestNamespace("Bar", new[]
                            {
                                TestClass("TestClass1", new[]
                                {
                                    _testCase1,
                                    _testCase2
                                })
                            }),

                            TestClass("TestClass2", new[]
                            {
                                _testCase3
                            })
                        })
                    });

            var actual = target.Children.TestCases()
                .Select(result => result.Test.Name)
                .ToArray();

            Assert.That(actual, Is.EquivalentTo(new[] { "Test1", "Test2", "Test3" }));
        }
    }
}