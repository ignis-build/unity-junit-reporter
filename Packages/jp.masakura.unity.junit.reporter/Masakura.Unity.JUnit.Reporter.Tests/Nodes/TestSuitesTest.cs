using System.Xml.Linq;
using Masakura.Unity.JUnit.Reporter.Nodes;
using NUnit.Framework;
using UnityEditor.TestTools.TestRunner.Api;
using Enumerable = System.Linq.Enumerable;

namespace Masakura.Unity.JUnit.Reporter.Tests.Nodes
{
    public sealed class TestSuitesTest
    {
        private MockTestResult _testCase1;

        [SetUp]
        public void SetUp()
        {
            _testCase1 = MockTestResult.TestCase("Test1", 0.892).WithPassed();
        }

        [Test]
        public void TestSimple()
        {
            var result = MockTestResult.TestSuites(new[]
            {
                MockTestResult.TestAssembly("1028", "Foo.Bar.dll",
                    6, 3, 2, 1, 10,
                    0.0335219, "2023-10-30T08:26:08",
                    new[]
                    {
                        MockTestResult.TestClass("Foo", new[]
                        {
                            _testCase1
                        })
                    })
            });

            var target = TestSuites.From(result);

            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@$"
<testsuites>
  <testsuite name=""Foo.Bar.dll"" tests=""6"" skipped=""2"" failures=""1"" errors=""0"" time=""0.0335219"" timestamp=""2023-10-30T08:26:08"" id=""1028"" package=""Foo.Bar.dll"">
    {Xml(_testCase1)}
  </testsuite>
</testsuites>
".Trim()));
        }

        [Test]
        public void TestMultipleAssemblies()
        {
            var result = MockTestResult.TestSuites(new[]
            {
                MockTestResult.TestAssembly("1028", "Foo.Bar.dll",
                    6, 3, 2, 1, 10,
                    0.0335219, "2023-10-30T08:26:08",
                    new[]
                    {
                        MockTestResult.TestClass("Foo", new[]
                        {
                            _testCase1
                        })
                    }),
                MockTestResult.TestAssembly("1029", "Test.dll",
                    10, 1, 2, 3, 4,
                    0.101, "2023-10-30T10:10:10",
                    Enumerable.Empty<MockTestResult>())
            });

            var target = TestSuites.From(result);

            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@$"
<testsuites>
  <testsuite name=""Foo.Bar.dll"" tests=""6"" skipped=""2"" failures=""1"" errors=""0"" time=""0.0335219"" timestamp=""2023-10-30T08:26:08"" id=""1028"" package=""Foo.Bar.dll"">
    {Xml(_testCase1)}
  </testsuite>
  <testsuite name=""Test.dll"" tests=""10"" skipped=""2"" failures=""3"" errors=""0"" time=""0.101"" timestamp=""2023-10-30T10:10:10"" id=""1029"" package=""Test.dll"" />
</testsuites>
".Trim()));
        }

        private static XElement Xml(ITestResultAdaptor testCase)
        {
            return TestCase.From(testCase).Xml();
        }
    }
}