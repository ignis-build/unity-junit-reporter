using Masakura.Unity.JUnit.Reporter.Nodes;
using NUnit.Framework;

namespace Masakura.Unity.JUnit.Reporter.Tests.Nodes
{
    public sealed class TestCaseTest
    {
        [Test]
        public void TestPass()
        {
            var testCase = MockTestResult.TestCase("Test1", 0.982)
                .WithPassed();
            MockTestResult.TestClass("My.TestClass1", new[] { testCase });

            var target = TestCase.From(testCase);

            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@"
<testcase name=""Test1"" classname=""My.TestClass1"" time=""0.982"" />
".Trim()));
        }

        [Test]
        public void TestFail()
        {
            var testCase = MockTestResult.TestCase("Test1", 0.982)
                .WithFailed("Assertion error message", "Call stack printed here");
            MockTestResult.TestClass("My.TestClass1", new[] { testCase });

            var target = TestCase.From(testCase);
            
            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@"
<testcase name=""Test1"" classname=""My.TestClass1"" time=""0.982"">
  <failure message=""Assertion error message"">Call stack printed here</failure>
</testcase>
".Trim()));
        }

        [Test]
        public void TestSkip()
        {
            var testCase = MockTestResult.TestCase("Test1", 0.982)
                .WithSkipped();
            MockTestResult.TestClass("My.TestClass1", new[] { testCase });

            var target = TestCase.From(testCase);
            
            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@"
<testcase name=""Test1"" classname=""My.TestClass1"" time=""0.982"">
  <skipped />
</testcase>
".Trim()));
        }
        
        [Test]
        public void TestInconclusive()
        {
            var testCase = MockTestResult.TestCase("Test1", 0.982)
                .WithInconclusive();
            MockTestResult.TestClass("My.TestClass1", new[] { testCase });

            var target = TestCase.From(testCase);

            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@"
<testcase name=""Test1"" classname=""My.TestClass1"" time=""0.982"" />
".Trim()));
        }
    }
}