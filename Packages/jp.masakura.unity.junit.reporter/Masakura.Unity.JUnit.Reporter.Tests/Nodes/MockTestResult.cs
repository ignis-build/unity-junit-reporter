using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework.Interfaces;
using UnityEditor.TestTools.TestRunner.Api;
using TestStatus = UnityEditor.TestTools.TestRunner.Api.TestStatus;

namespace Masakura.Unity.JUnit.Reporter.Tests.Nodes
{
    internal sealed class MockTestResult : ITestResultAdaptor
    {
        public TNode ToXml()
        {
            throw new NotSupportedException();
        }

        public ITestAdaptor Test { get; private set; }
        public string Name => throw new NotSupportedException();
        public string FullName { get; private set; }
        public string ResultState => throw new NotSupportedException();
        public TestStatus TestStatus { get; private set; }
        public double Duration { get; private set; }
        public DateTime StartTime { get; private set; }
        public DateTime EndTime => throw new NotSupportedException();
        public string Message { get; private set; }
        public string StackTrace { get; private set; }
        public int AssertCount => throw new NotSupportedException();
        public int FailCount { get; private set; }
        public int PassCount { get; private set; }
        public int SkipCount { get; private set; }
        public int InconclusiveCount { get; private set; }
        public bool HasChildren => Children.Any();
        public IEnumerable<ITestResultAdaptor> Children { get; private set; } = Enumerable.Empty<ITestResultAdaptor>();
        public string Output => throw new NotSupportedException();

        public static MockTestResult TestCase(string testCaseName, double duration)
        {
            return new MockTestResult
            {
                Duration = duration,
                Test = MockTest.TestCase(testCaseName)
            };
        }

        public static MockTestResult TestClass(string className, IEnumerable<MockTestResult> children)
        {
            return new MockTestResult
            {
                FullName = className,
                Test = MockTest.TestClass(className)
            }.WithChildren(children);
        }


        public static MockTestResult TestAssembly(string id,
            string assemblyName,
            int testCaseCount,
            int passCount,
            int skipCount,
            int failCount,
            int inconclusiveCount,
            double duration,
            string startTime,
            IEnumerable<MockTestResult> children)
        {
            return new MockTestResult
            {
                PassCount = passCount,
                SkipCount = skipCount,
                FailCount = failCount,
                InconclusiveCount = inconclusiveCount,
                Duration = duration,
                StartTime = DateTime.Parse(startTime),
                Test = MockTest.TestAssembly(id, testCaseCount, assemblyName)
            }.WithChildren(children);
        }

        public static MockTestResult TestNamespace(string @namespace, IEnumerable<MockTestResult> children)
        {
            return new MockTestResult
            {
                Test = MockTest.TestNamespace(@namespace)
            }.WithChildren(children);
        }

        private MockTestResult WithParent(ITestResultAdaptor parent)
        {
            Test = ((MockTest)Test).WithParent(parent.Test);
            return this;
        }

        public static MockTestResult TestSuites(IEnumerable<MockTestResult> results)
        {
            return new MockTestResult().WithChildren(results);
        }

        public MockTestResult WithFailed(string assertionErrorMessage, string stackTrace)
        {
            Message = assertionErrorMessage;
            StackTrace = stackTrace;
            TestStatus = TestStatus.Failed;

            return this;
        }

        public MockTestResult WithSkipped()
        {
            TestStatus = TestStatus.Skipped;
            return this;
        }

        public MockTestResult WithInconclusive()
        {
            TestStatus = TestStatus.Inconclusive;
            return this;
        }

        public MockTestResult WithPassed()
        {
            TestStatus = TestStatus.Passed;
            return this;
        }

        private MockTestResult WithChildren(IEnumerable<MockTestResult> children)
        {
            Children = children.Select(x => x.WithParent(this)).ToArray();
            return this;
        }
    }
}