﻿using System.Xml.Linq;
using Masakura.Unity.JUnit.Reporter.Nodes;
using NUnit.Framework;
using UnityEditor.TestTools.TestRunner.Api;
using static Masakura.Unity.JUnit.Reporter.Tests.Nodes.MockTestResult;
using TestCase = Masakura.Unity.JUnit.Reporter.Nodes.TestCase;

namespace Masakura.Unity.JUnit.Reporter.Tests.Nodes
{
    public sealed class TestSuiteTest
    {
        private MockTestResult _testCase1;
        private MockTestResult _testCase2;
        private MockTestResult _testCase3;

        [SetUp]
        public void SetUp()
        {
            _testCase1 = TestCase("Test1", 0.982).WithPassed();
            _testCase2 = TestCase("Test2", 0.102).WithPassed();
            _testCase3 = TestCase("Test3", 0.103).WithPassed();
        }

        [Test]
        public void TestSimple()
        {
            var testResult =
                TestAssembly("1028", "Foo.Bar.dll",
                    6, 3, 2, 1, 10,
                    0.0335219, "2023-10-30T08:26:08",
                    new[]
                    {
                        TestClass("TestClass1", new[]
                        {
                            _testCase1
                        })
                    });

            var target = TestSuite.From(testResult);

            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@$"
<testsuite name=""Foo.Bar.dll"" tests=""6"" skipped=""2"" failures=""1"" errors=""0"" time=""0.0335219"" timestamp=""2023-10-30T08:26:08"" id=""1028"" package=""Foo.Bar.dll"">
  {Xml(_testCase1)}
</testsuite>
".Trim()));
        }

        [Test]
        public void TestFlatten()
        {
            var testResult =
                TestAssembly("1028", "Foo.Bar.dll",
                    6, 3, 2, 1, 10,
                    0.0335219, "2023-10-30T08:26:08",
                    new[]
                    {
                        TestNamespace("Foo", new[]
                        {
                            TestNamespace("Bar", new[]
                            {
                                TestClass("TestClass1", new[]
                                {
                                    _testCase1,
                                    _testCase2
                                })
                            }),

                            TestClass("TestClass2", new[]
                            {
                                _testCase3
                            })
                        })
                    });

            var target = TestSuite.From(testResult);

            Assert.That(target.Xml().ToString(),
                Is.EqualTo(@$"
<testsuite name=""Foo.Bar.dll"" tests=""6"" skipped=""2"" failures=""1"" errors=""0"" time=""0.0335219"" timestamp=""2023-10-30T08:26:08"" id=""1028"" package=""Foo.Bar.dll"">
  {Xml(_testCase1)}
  {Xml(_testCase2)}
  {Xml(_testCase3)}
</testsuite>
".Trim()));
        }

        private static XElement Xml(ITestResultAdaptor testCase)
        {
            return TestCase.From(testCase).Xml();
        }
    }
}