using System.Xml.Linq;
using UnityEditor.TestTools.TestRunner.Api;

namespace Masakura.Unity.JUnit.Reporter.Nodes
{
    public sealed class FailureTestCaseChild : ITestCaseChild
    {
        private readonly ITestResultAdaptor _result;

        private FailureTestCaseChild(ITestResultAdaptor result)
        {
            _result = result;
        }

        private string Message => _result.Message;
        private string StackTrace => _result.StackTrace;

        public XElement Xml()
        {
            return new XElement("failure",
                new XAttribute("message", Message),
                StackTrace);
        }

        public static FailureTestCaseChild Create(ITestResultAdaptor result)
        {
            if (result.TestStatus == TestStatus.Failed)
                return new FailureTestCaseChild(result);

            return null;
        }
    }
}