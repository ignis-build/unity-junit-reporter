using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.TestTools.TestRunner.Api;

namespace Masakura.Unity.JUnit.Reporter.Nodes
{
    public static class Enumerable
    {
        public static IEnumerable<ITestResultAdaptor> TestCases(this IEnumerable<ITestResultAdaptor> source)
        {
            return source
                .Flatten(result => result.Children)
                .Where(result => result.Test.IsTestCase());
        }

        private static IEnumerable<T> Flatten<T>(this IEnumerable<T> source, Func<T, IEnumerable<T>> collectionSelector)
        {
            foreach (var element in source)
            {
                yield return element;

                foreach (var child in collectionSelector(element).Flatten(collectionSelector)) yield return child;
            }
        }

        private static bool IsTestCase(this ITestAdaptor test)
        {
            return !(test.IsSuite || test.IsTestAssembly);
        }
    }
}