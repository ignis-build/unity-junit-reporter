using System.Xml.Linq;
using UnityEditor.TestTools.TestRunner.Api;

namespace Masakura.Unity.JUnit.Reporter.Nodes
{
    public sealed class SkipTestCaseChild : ITestCaseChild
    {
        private SkipTestCaseChild()
        {
        }

        private static SkipTestCaseChild Instance { get; } = new();

        public XElement Xml()
        {
            return new XElement("skipped");
        }

        public static SkipTestCaseChild Create(ITestResultAdaptor result)
        {
            if (result.TestStatus == TestStatus.Skipped)
                return Instance;
            return null;
        }
    }
}