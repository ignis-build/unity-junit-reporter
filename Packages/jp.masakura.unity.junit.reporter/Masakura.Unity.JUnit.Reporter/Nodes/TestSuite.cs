using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnityEditor.TestTools.TestRunner.Api;

namespace Masakura.Unity.JUnit.Reporter.Nodes
{
    public sealed class TestSuite
    {
        private readonly ITestResultAdaptor _result;

        private TestSuite(ITestResultAdaptor result)
        {
            _result = result;
        }

        private string Name => _result.Test.Name;
        private int Tests => _result.Test.TestCaseCount;
        private int Skipped => _result.SkipCount;
        private int Failures => _result.FailCount;
        // ReSharper disable once MemberCanBeMadeStatic.Local
        private int Errors => 0;
        private double Time => _result.Duration;
        private DateTime Timestamp => _result.StartTime;
        private string Id => _result.Test.Id;
        private string Package => Name;

        private IEnumerable<TestCase> TestCases => _result.Children.TestCases()
            .Select(TestCase.From);

        public XElement Xml()
        {
            return new XElement("testsuite",
                new XAttribute("name", Name),
                new XAttribute("tests", Tests),
                new XAttribute("skipped", Skipped),
                new XAttribute("failures", Failures),
                new XAttribute("errors", Errors),
                new XAttribute("time", Time),
                new XAttribute("timestamp", Timestamp),
                new XAttribute("id", Id),
                new XAttribute("package", Package),
                TestCases.Select(testCase => testCase.Xml()));
        }

        public static TestSuite From(ITestResultAdaptor result)
        {
            return new TestSuite(result);
        }
    }
}