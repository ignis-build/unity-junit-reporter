using System.Xml.Linq;

namespace Masakura.Unity.JUnit.Reporter.Nodes
{
    public sealed class PassTestCaseChild : ITestCaseChild
    {
        private PassTestCaseChild()
        {
        }

        public static PassTestCaseChild Instance { get; } = new();

        public XElement Xml()
        {
            return null;
        }
    }
}