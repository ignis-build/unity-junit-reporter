using Masakura.Unity.JUnit.Reporter.Files;
using UnityEditor.TestTools.TestRunner.Api;

namespace Masakura.Unity.JUnit.Reporter
{
    /// <summary>
    ///     JUnit report exporter for <see cref="TestRunnerApi" />.
    /// </summary>
    public sealed class JUnitReporter : ICallbacks
    {
        private readonly IReportFileNameResolver _fileNameResolver;

        /// <summary>
        ///     The output filename is `TestResults-{Ticks}.xml`.
        /// </summary>
        public JUnitReporter() : this(IReportFileNameResolver.Instance)
        {
        }

        /// <summary>
        ///     The report will be output to the specified file.
        /// </summary>
        /// <param name="reportFilename"></param>
        // ReSharper disable once UnusedMember.Global
        public JUnitReporter(string reportFilename) :
            this(new SpecifiedFileNameResolver(reportFilename))
        {
        }

        // ReSharper disable once MemberCanBePrivate.Global
        public JUnitReporter(IReportFileNameResolver fileNameResolver)
        {
            _fileNameResolver = fileNameResolver;
        }

        public void RunStarted(ITestAdaptor testsToRun)
        {
        }

        public void RunFinished(ITestResultAdaptor result)
        {
            var report = JUnitReport.From(result);
            report.Save(_fileNameResolver.Resolve());
        }

        public void TestStarted(ITestAdaptor test)
        {
        }

        public void TestFinished(ITestResultAdaptor result)
        {
        }
    }
}