using System;

namespace Masakura.Unity.JUnit.Reporter.Times
{
    public abstract class TimeProvider
    {
        public abstract DateTimeOffset GetLocalNow();

        public static TimeProvider Instance => TimeProviderImp.Instance;
    }
}