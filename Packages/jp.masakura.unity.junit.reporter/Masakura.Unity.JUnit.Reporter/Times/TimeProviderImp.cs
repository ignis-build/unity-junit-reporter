using System;

namespace Masakura.Unity.JUnit.Reporter.Times
{
    internal sealed class TimeProviderImp : TimeProvider
    {
        private TimeProviderImp()
        {
        }

        public new static TimeProviderImp Instance { get; } = new();

        public override DateTimeOffset GetLocalNow()
        {
            return DateTimeOffset.Now;
        }
    }
}