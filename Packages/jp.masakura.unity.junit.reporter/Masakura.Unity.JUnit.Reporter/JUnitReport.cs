using Masakura.Unity.JUnit.Reporter.Nodes;
using UnityEditor.TestTools.TestRunner.Api;

namespace Masakura.Unity.JUnit.Reporter
{
    public sealed class JUnitReport
    {
        private readonly TestSuites _testSuites;

        private JUnitReport(TestSuites testSuites)
        {
            _testSuites = testSuites;
        }

        public static JUnitReport From(ITestResultAdaptor result)
        {
            return new JUnitReport(TestSuites.From(result));
        }

        public void Save(string fileName)
        {
            _testSuites.Xml().Save(fileName);
        }
    }
}