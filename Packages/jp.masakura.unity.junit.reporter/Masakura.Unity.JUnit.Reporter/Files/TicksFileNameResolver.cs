using Masakura.Unity.JUnit.Reporter.Times;

namespace Masakura.Unity.JUnit.Reporter.Files
{
    public sealed class TicksFileNameResolver : IReportFileNameResolver
    {
        private readonly TimeProvider _timeProvider;

        public TicksFileNameResolver(TimeProvider timeProvider)
        {
            _timeProvider = timeProvider;
        }

        public static IReportFileNameResolver Instance { get; } =
            new TicksFileNameResolver(TimeProvider.Instance);

        public string Resolve()
        {
            return $"TestResults-junit-{_timeProvider.GetLocalNow().Ticks}.xml";
        }
    }
}