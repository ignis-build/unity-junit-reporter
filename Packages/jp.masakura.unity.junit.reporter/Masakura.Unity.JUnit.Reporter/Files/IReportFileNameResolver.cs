namespace Masakura.Unity.JUnit.Reporter.Files
{
    public interface IReportFileNameResolver
    {
        static IReportFileNameResolver Instance => TicksFileNameResolver.Instance;

        string Resolve();
    }
}