namespace Masakura.Unity.JUnit.Reporter.Files
{
    public sealed class SpecifiedFileNameResolver : IReportFileNameResolver
    {
        private readonly string _fileName;

        public SpecifiedFileNameResolver(string fileName)
        {
            _fileName = fileName;
        }

        public string Resolve()
        {
            return _fileName;
        }
    }
}