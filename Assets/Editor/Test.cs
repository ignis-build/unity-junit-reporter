using Editor.Reporting;
using Masakura.Unity.JUnit.Reporter;
using UnityEditor;
using UnityEditor.TestTools.TestRunner.Api;
using UnityEngine;

// ReSharper disable once CheckNamespace
[InitializeOnLoad]
public static class Test
{
    static Test()
    {
        Runner = ScriptableObject.CreateInstance<TestRunnerApi>();
        Runner.RegisterCallbacks(new JUnitReporter());
        Runner.RegisterCallbacks(IErrorAsFailed.Instance);
    }

    private static TestRunnerApi Runner { get; }

    [MenuItem("Build/Test Edit Mode")]
    public static void EditMode()
    {
        Runner.Execute(new ExecutionSettings(new Filter { testMode = TestMode.EditMode }));
    }
}