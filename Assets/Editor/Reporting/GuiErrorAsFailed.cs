using UnityEditor.TestTools.TestRunner.Api;

namespace Editor.Reporting
{
    internal class GuiErrorAsFailed : IErrorAsFailed
    {
        public void RunStarted(ITestAdaptor testsToRun)
        {
        }

        public void RunFinished(ITestResultAdaptor result)
        {
            var fails = result.FailCount;
            if (fails > 0) throw new TestFailedException(fails);
        }

        public void TestStarted(ITestAdaptor test)
        {
        }

        public void TestFinished(ITestResultAdaptor result)
        {
        }
    }
}