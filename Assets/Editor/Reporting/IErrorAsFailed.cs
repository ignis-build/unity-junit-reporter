using UnityEditor.TestTools.TestRunner.Api;
using UnityEngine;

namespace Editor.Reporting
{
    internal interface IErrorAsFailed : ICallbacks
    {
        private static readonly BatchModeErrorAsFailed BatchMode = new();
        private static readonly GuiErrorAsFailed GUI = new();

        public static IErrorAsFailed Instance
        {
            get
            {
                if (Application.isBatchMode) return BatchMode;
                return GUI;
            }
        }
    }
}