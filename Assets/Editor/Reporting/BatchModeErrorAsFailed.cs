using System;
using UnityEditor;
using UnityEditor.TestTools.TestRunner.Api;
using UnityEngine;

namespace Editor.Reporting
{
    internal sealed class BatchModeErrorAsFailed : IErrorAsFailed
    {
        private readonly GuiErrorAsFailed _parent = new();
        
        public void RunStarted(ITestAdaptor testsToRun)
        {
        }

        public void RunFinished(ITestResultAdaptor result)
        {
            try
            {
                _parent.RunFinished(result);
            }
            catch (Exception ex)
            {
                Debug.LogException(ex);
                EditorApplication.Exit(1);
                return;
            }
            
            EditorApplication.Exit(0);
        }

        public void TestStarted(ITestAdaptor test)
        {
        }

        public void TestFinished(ITestResultAdaptor result)
        {
        }
    }
}