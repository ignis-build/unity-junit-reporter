using System;

namespace Editor.Reporting
{
    internal sealed class TestFailedException : Exception
    {
        private TestFailedException(string message) : base(message)
        {
        }

        public TestFailedException(int failCount) : this($"{failCount} tests has failed")
        {
        }
    }
}